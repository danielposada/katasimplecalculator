function ThrowIfArgsAreNull(arg1, arg2){
    if (arg1 === null || arg2 === null) {
        throw new Error("Nullargument Exception")
    }
}

function SimpleCalc() {
}

function Adder() {
}

Adder.prototype.Add = function(/* args */) {
    var args = Array.prototype.slice.call(arguments);
    var acc = 0;
    for(var i in args) {
        var num = args[i];
        if (num === null) {
            throw new Error("Nullargument Exception");
        }
        if (typeof(num) != 'number') {
            return NaN;
        }
        acc += args[i];
}
    return acc;
}

function Subtracter() {

}
Subtracter.prototype.Subtract = function(a,b) {
    return a - b;
}


function Divider() {
}

Divider.prototype.Divide = function(a,b) {
    ThrowIfArgsAreNull(a,b);
    if((typeof(a) != 'number') || (typeof(b) != 'number')){
        return NaN;
    }
    if(b === 0){
        throw new Error("Divide by zero")
    }
    return a / b;
}

function Multiplier() {
}

Multiplier.prototype.Multiply = function(a,b) {
    ThrowIfArgsAreNull(a,b);
    if((typeof(a) != 'number') || (typeof(b) != 'number')){
        return NaN;
    }

    return a * b;
}


SimpleCalc.prototype = {};
SimpleCalc.prototype.Add = Adder.prototype.Add;
SimpleCalc.prototype.Subtract = Subtracter.prototype.Subtract;
SimpleCalc.prototype.Multiply = Multiplier.prototype.Multiply;
SimpleCalc.prototype.Divide = Divider.prototype.Divide;

exports.SimpleCalc = SimpleCalc;
exports.Adder = Adder;
exports.Subtracter = Subtracter;
exports.Divider = Divider;
exports.Multiplier = Multiplier;