What did we learn?
==================
- How to use Mocha, and how to do TDD with JavaScript
- About prototypes in JavaScript
 -- For more, check out Matt's markdown file, oop-dojo.md
- JavaScript version of C# params
- About method calls on instances vs prototypes
- Prototypes help performance
- Simple problems are good for the 2-hour dojo

What impeded us from learning?
==============================
- Not being able to have the code and tests in separate files
- We didn't do enough deliberate refactoring
- We spent too much time in the intro before getting to the code
 -- Participants should be reading the tutorials, etc. ahead of time
- There was no documentation on how to setup your own environment

Other ideas
===========
- SharePoint dojo (Duffy)
- CoffeeScript dojo
- We should make sure to specifically refactor as part of the TDD loop
- Refactoring dojo
- Java dojo (Alan)
- Python dojo (Danny)
 -- Django?
 -- Refactoring?
- We could commit and push to BitBucket while we code
- Git dojo
