var assert = require('assert');
var SimpleCalc = require('./SimpleCalc.js');
/*
describe('Array', function() {
    describe('#indexOf()', function() {
        it('should return -1 when the value is not present', function() {
            assert.equal(-1, [1, 2, 3].indexOf(5));
            assert.equal(-1, [1, 2, 3].indexOf(0));
        })
    })
})
*/

describe('Adder', function() {
    var target = new SimpleCalc.Adder();
    describe('Add_Adds_two_numbers', function() {
        it('should add num1 to num2', function() {
            var num1 = 1;
            var num2 = 2;
            assert.equal(num1 + num2, target.Add(num1, num2));
        });
        it('should puke if first arg is a string', function() {
            var num1 = '1';
            var num2 = 2;
            //assert.equal(NaN, target.Add(num1, num2));
            assert.equal(true,isNaN(target.Add(num1, num2)));
        });
        it('should puke if second arg is a string', function() {
            var num1 = 1;
            var num2 = '12.3';
            //assert.equal(NaN, target.Add(num1, num2));
            assert.equal(true,isNaN(target.Add(num1, num2)));
        });
        it('should puke if second arg is null', function() {
            var num1 = 1;
            var num2 = null;
            //assert.equal(NaN, target.Add(num1, num2));
            //assert.equal(true,isNaN(target.Add(num1, num2)));
            assert.throws(function() { 
                target.Add(num1, num2) 
            }, Error);
        });
        it('should add num1 to num2', function() {
            var num1 = 1;
            var num2 = 2;
            assert.equal(num1 + num2, target.Add(num1, num2));
        });
    });
    describe('Add.Add can add three numbers', function() {
        it('should take three numbers and add them', function() {
            assert.equal(6, target.Add(1, 2 ,3));
        })
    })
});

describe('Subtracter', function() {
    var target = new SimpleCalc.Subtracter();
    describe('Subtracter.subtract() subtracts two numbers and returns result', function() {
        it('should subtract num2 from num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 - num1, target.Subtract(num2, num1));
        })
    })
})

describe('Divider', function() {
    var target = new SimpleCalc.Divider();
    describe('Divider.Divide() divides two numbers and returns result', function() {
        it('should divide num2 by num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 / num1, target.Divide(num2, num1));
        });
    });
    describe('Divider.Divide() divides two numbers and returns result', function() {
        it('should throw zero division error', function() {
            var num1 = 0;
            var num2 = 10;
            assert.throws(function(){
                target.Divide(num2, num1)
            }, Error)
        });
    });
});

describe('Multiply', function() {
    var target = new SimpleCalc.Multiplier();
    describe('Multiplier.Multiply multiplies two numbers and returns the result', function() {
        it('should multiply num2 and num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 * num1, target.Multiply(num2, num1));
        });
    });
});

describe('SimpleCalc', function() {
    var target = new SimpleCalc.SimpleCalc();
    describe('Add_Adds_two_numbers',function() {
        it('should add num1 to num2', function() {
            var num1 = 1;
            var num2 = 2;
            assert.equal(num1 + num2, target.Add(num1, num2));
        });
    });
    describe('Subtracter.Subtract subtracts two numbers', function() {
        it('should subtract num2 from num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 - num1, target.Subtract(num2, num1));
        })
    });
    describe('Multiplier multiplies two numbers', function() {
        it('should multiply num2 by num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 * num1, target.Multiply(num2, num1));
        })
    });
    describe('Divider divides two numbers', function() {
        it('should divide num2 by num1', function() {
            var num1 = 20;
            var num2 = 10;
            assert.equal(num2 / num1, target.Divide(num2, num1));
        })
    });
});
