Simple Calculator Kata

This kata will be an exercise to build a simple calculator in JavaScript. No graphical UI will be created; it will be command line / console driven UI.

Imagine we have four interfaces (JavaScript classes):

1. Adder
2. Multiplier
3. Subtracter
4. Divider

These JavaScript classes each provide an API for their corresponding mathematical operation (Adder adds two numbers, Multiplier multiplies two numbers, etc).

The goal of the kata is to create a JavaScript class called SimpleCalculator that implements the functions provided from the four classes above.
